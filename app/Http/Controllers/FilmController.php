<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tahun = date('Y');
        $messages = [
            'required' => ':attribute wajib diisi.',
            'max' => ':attribute maksimum :max.',
        ];
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => "required|numeric|max:$tahun",
            'genre_id' => 'required',
            'poster' => 'required|mimes:jpg,jpeg,png|max:2200'
        ], $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $poster = $request->file('poster');
        // jika poster nya gaada
        if (!$poster) {
            return redirect()
                ->back()
                ->withErrors(['poster' => "Harap masukkan file!"])
                ->withInput();
            // sebenernya ngasih required disana udah cukup, mungkin jika posternya gaada kamu mau melakukan sesuatu, aku kasih caranya
        } else {
            // jika posternya ada
            $nama_file = uniqid('img_') . $poster->getClientOriginalName();
            // file nya mau ditaro kemana,
            $lokasi_file = public_path() . '/uploads/film/';
            $poster->move($lokasi_file, $nama_file);

            $film = Film::create([
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id,
                'poster' => $nama_file,
            ]);
        }

        return redirect()->route('film.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::all();
        $film = Film::find($id);
        return view('film.edit', compact('film', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // update data
        // bikin validasinya dulu,
        $film = Film::find($id);
        $tahun = date('Y');
        $messages = [
            'required' => ':attribute wajib diisi.',
            'max' => ':attribute maksimum :max.',
        ];
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => "required|numeric|max:$tahun",
            'genre_id' => 'required',
            'poster' => 'mimes:jpg,jpeg,png|max:2200' //gaperlu required, siapa tau gamau ganti poster.
        ], $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $film->update([  //sama aja
            'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'genre_id' => $request->genre_id,
        ]);

        // cek image
        $poster = $request->file('poster');
        if ($poster) {
            // jika ada poster, kalo gaada ya biarin aja
            // jika posternya ada
            $nama_file = uniqid('img_') . $poster->getClientOriginalName();
            // file nya mau ditaro kemana,
            $lokasi_file = public_path() . '/uploads/film/';
            $poster->move($lokasi_file, $nama_file); //ini kan buat mindahin file baru

            unlink($lokasi_file . $film->poster); //public/uploads/film/nama_file.jpg . ini buat menghapus file yg lama

            $film->update(['poster' => $nama_file]);
        }
        return redirect()->route('film.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        Film::destroy($id);
        return redirect()->route('film.index');
    }
}
