<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.index');
    }

    public function kirim(Request $request)
    {
        $namadepan = $request->firstname;
        $namabelakang = $request->lastname;
        $jk = $request->gender;
        $kebangsaan = $request->nasional;
        $bahasa = $request->bahasa;
        $biodata = $request->bio;

        return view('welcome.index', compact('namadepan', 'namabelakang', 'jk', 'kebangsaan', 'bahasa', 'biodata'));
    }
}
