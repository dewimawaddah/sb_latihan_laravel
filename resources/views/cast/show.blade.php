@extends('layouts.master')

@section('judul')
<h2>Show Cast dengan ID {{$cast->id}}</h2>
@endsection

@section('konten')
<h4>Nama : {{$cast->nama}}</h4>
<h4>Umur : {{$cast->umur}}</h4>
<h4>Bio : {{$cast->bio}}</h4>
@endsection
