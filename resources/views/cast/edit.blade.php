@extends('layouts.master')

@section('judul')
Halaman Update Cast
@endsection

@section('konten')
<div>
    <h2>Update Data Cast</h2>
    <form action="/cast/{{ $cast->id }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama cast"
                value="{{ $cast->nama }}">
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" placeholder="Masukkan umur cast"
                value="{{ $cast->umur }}">
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio" placeholder="Masukkan bio cast" value="{{ $cast->bio }}">
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection
