@extends('layouts.master')

@section('judul')
Halaman Tambah Film
@endsection

@section('konten')

<h2>Tambah Data Film</h2>
<form action="{{ route('film.store') }}" enctype="multipart/form-data" method="POST">
    @csrf
    <div class="form-group">
        <label for="judul">Judul Film</label>
        <input type="text" class="form-control" name="judul" placeholder="Masukkan judul film">
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="ringkasan">Ringkasan</label>
        <textarea class="form-control" name="ringkasan" cols="30" placeholder="Masukkan ringkasan"></textarea>
        @error('ringkasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="tahun">Tahun</label>
        {{-- <textarea class="form-control" name="tahun" cols="30" placeholder="Masukkan tahun"></textarea> --}}
        {{-- <input type="number" min="1950" max="{{date('Y')}}" name="tahun" id="tahun" class="form-control"> --}}
        {{-- <select name="" id="" class="form-control">
            @for ($i = date('Y'); $i >= 1950; $i--)
            <option>{{$i}}</option>
            @endfor
        </select> --}}

        <input type="text" class="yearpicker form-control" value="" name="tahun" />
        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Genre</label>
        <select class="form-control" name="genre_id">
            <option value=""> -- pilih Genre --</option>
            @foreach ($genre as $genre)
            <option value="{{ $genre->id }}">{{ $genre->nama }}</option>
            @endforeach
        </select>
        @error('genre_id')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control-file" name="poster">
        @error('poster')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>
@endsection


@push('style')
<link rel="stylesheet" href="{{asset('admin/plugins/yearpicker/yearpicker.css')}}">
@endpush

@push('script')
<script src="{{asset('admin/plugins/yearpicker/yearpicker.js')}}" async></script>
@endpush
