@extends('layouts.master')

@section('judul')
Halaman List Film
@endsection

@section('konten')
<a href="film/create" class="btn btn-primary mb-3">Tambah Data Film</a>

<div class="row">
    @foreach ($film as $film)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{ asset('uploads/film/'.$film->poster) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3 class="card-title mb-3">{{ $film->judul }}</h3>
                <p class="card-text">{{ $film->ringkasan }}</p>

                {{-- Detail --}}
                <a href="{{ route('film.show', ['film'=>$film->id]) }}" class="btn btn-primary">Detail</a>
                {{-- EDIT --}}
                {{-- <a href="/film/{{ $film->id }}/edit" class="btn btn-info">Edit</a> --}}
                <a href="{{ route('film.edit', ['film'=>$film->id]) }}" class="btn btn-info">Edit</a>
                <form method="POST" action="{{ route('film.destroy', ['film'=>$film->id]) }}" class="d-inline">
                    <button class="btn btn-danger" type="submit">Hapus</button>
                    @csrf
                    @method('DELETE')
                </form>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection
