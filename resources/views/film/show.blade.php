@extends('layouts.master')

@section('judul')
Halaman List Film
@endsection

@section('konten')

<img src="{{ asset('uploads/film/'. $film->poster) }}" width="300">
<h2 class="mt-3">{{ $film->judul }}</h2>
<p>{{ $film->ringkasan }}</p>
@endsection
