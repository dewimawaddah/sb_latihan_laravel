@extends('layouts.master')

@section('judul')
Halaman Register
@endsection

@section('konten')

<h2>Buat Account Baru</h2>

<form action="/welcome" method="POST">
    @csrf
    <label>Nama Depan :</label> <br><br>
    <input type="text" name="firstname"> <br><br>

    <label>Nama Belakang :</label> <br><br>
    <input type="text" name="lastname"> <br><br>

    <label>Jenis Kelamin:</label> <br><br>
    <input type="radio" name="gender">Male <br>
    <input type="radio" name="gender">Female <br>
    <input type="radio" name="gender">Other <br><br>

    <label>Kebangsaan</label> <br><br>
    <select name="nasional" id="">
        <option value="Indonesia">Indonesia</option>
        <option value="Jerman">Jerman</option>
    </select> <br><br>

    <label>Bahasa</label> <br><br>
    <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
    <input type="checkbox" name="bahasa"> Inggris <br>
    <input type="checkbox" name="bahasa"> Other <br><br>

    <label>Bio:</label> <br><br>
    <textarea name="bio" cols="30" rows="10"></textarea> <br><br>

    <button type="Submit" value="simpan">Simpan</button>

</form>
@endsection
