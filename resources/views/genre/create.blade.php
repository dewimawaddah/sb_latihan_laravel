@extends('layouts.master')

@section('judul')
Halaman Tambah Genre
@endsection

@section('konten')

<h2>Tambah Data Genre</h2>
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan nama cast">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>
@endsection
