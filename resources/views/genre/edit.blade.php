@extends('layouts.master')

@section('judul')
Edit Data Genre
@endsection

@section('konten')

<h2>Edit Data Genre</h2>
<form action="/genre/{{$genre->id}}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan nama cast" value="{{$genre->nama}}">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form>
</div>
@endsection
